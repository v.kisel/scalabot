const express = require('express');
const robot = require('robotjs');
const async = require('async');
const fs = require('fs');
const jsonfile = require('jsonfile');
// console.log('color', robot.getPixelColor(300, 300));
const router = express.Router();
const path = require('path');
const storage = require('../modules/storage');
const map = require('../modules/map');
const play = require('../modules/play');
const key_taps = require("../modules/keys");

let timer = setTimeout(() => {
}, 0);

router.get('/', (req, res) => {
    res.render('index', {
        title: 'scalabot',
        functions: Object.keys(robot).concat('wait'),
        key_taps: key_taps,
        recipes: fs.readdirSync(storage.pathTo('recipes')),
        scenarios: fs.readdirSync(storage.pathTo('scenarios')),
        csv: fs.readdirSync(storage.pathTo('csv'))
    })
});
router.get('/:dir(scenario|recipe|csv)/:file', (req, res) => {
    let dir = req.params.dir;
    let file = req.params.file;
    let load = `load${dir[0].toUpperCase() + dir.slice(1).toLowerCase()}`;
    let scalajson = storage[load](file);
    res.json(scalajson || '');
});
router.post('/play', (req, res) => {
    let func = req.body.func;
    let params = req.body.params;
    play.commands([{func, params}], status => {
        io.log('play', status.name, status.data);
        if (status.name === 'ended') {
            res.end('Done');
        }
    });
});
router.post('/recipe-play', (req, res) => {
    let scalajson = storage.loadRecipe(req.body.file);
    play.commands(scalajson.records, status => {
        io.log('play', status.name, status.data);
        if (status.name === 'ended') {
            res.end('Done');
        }
    });
    res.end('Done');
});
router.post('/scenario-play', (req, res) => {
    let poScenario = storage.loadScenario(req.body.file);
    let poCSV = storage.loadCsv(poScenario.csv);

    async.series([
        done => {
            if (!poScenario.start) {
                return done();
            }
            let recipe = storage.loadRecipe(poScenario.start);

            if (!recipe) {
                return done();
            }
            io.log('play', recipe.records, poScenario.start);
            play.commands(recipe.records, status => {
                io.log('play', status.name, status.data);
                if (status.name === 'ended') {
                    done();
                }
            });
        },
        done => {
            let recipies = storage.loadReceipeHash(poScenario);
            let entities = map.recordsByEntity(poCSV, recipies, poScenario.flatten, poScenario.transitions);


            async.mapSeries(entities, (entity, done) => {
                play.commands(entity.commands, status => {
                    io.log('play', status.name, status.data);
                    emitMouse();
                    if (status.name === 'ended') {
                        done();
                    }
                });
            }, done);
        },
        done => {
            if (!poScenario.end) {
                return done();
            }

            let recipe = storage.loadRecipe(poScenario.end);
            if (!recipe) {
                return done();
            }
            play.commands(recipe.records, status => {
                io.log('play', status.name, status.data);
                if (status.name === 'ended') {
                    done();
                }
            });
        }
    ]);

    res.end('Done');
});
router.post('/scenario/:file', (req, res) => {
    storage.writeScenario(req.params.file, req.body.scenario);
    res.end('Ended');
});
router.post('/stop', (req, res) => {
    clearTimeout(timer);
    res.end('Done');
});
router.post('/recipe', (req, res) => {
    storage.writeRecipe(req.body.file, req.body);
    res.end();
});
router.post('/csv/:file', (req, res) => {
    storage.writeCsv(req.params.file, req.body.csv);
    res.end();
});
router.delete('/recipe', (req, res) => {
    let scalafile = storage.pathTo('recipes', req.body.file);
    fs.unlink(scalafile);
    res.end('Not yet implemented');
});

function emitMouse() {
    // Get mouse position.
    let mouse = robot.getMousePos();

    // Get pixel color in hex format.
    let hex = robot.getPixelColor(mouse.x, mouse.y);
    io.emit('mouse', "#" + hex + ";pos:" + mouse.x + "," + mouse.y);
}

module.exports = router;
