'use strict'

const express = require('express');
const robot = require('robotjs');
const async = require('async');
const fs = require('fs');
const jsonfile = require('jsonfile');
// console.log('color', robot.getPixelColor(300, 300));
const router = express.Router();
const path = require('path');
const storage = require('../modules/storage');
const map = require('../modules/map');
const play = require('../modules/play');
const key_taps = require("../modules/keys");
const checkAuth = require("../modules/auth");
const _ = require("underscore")
const db = require('../server/db')

let timer = setTimeout(() => {}, 0);

router.get("/login", (req, res) => {
    res.render('login', {
        title: 'login'
    })
});

router.post("/login", function login(req, res, next) {
    var params = req.body
    db.User.findOne({
        username: params.username
    }, (err, data) => {
        if (err) {
            res.send(err)
        } else if (data && data.password === params.password) {
            req.session.user = data
            res.redirect('/')
        } else {
            res.redirect('/login?error=loginFailed')
        }
    })
})

router.get("/logout", function logout(req, res) {
    req.session.destroy();
    res.redirect("/login");
})

router.get('/', checkAuth, (req, res) => {
    res.render('index', {
        title: 'scalabot',
        recipes: fs.readdirSync(storage.pathTo('recipes')),
        scenarios: fs.readdirSync(storage.pathTo('scenarios')),
        csv: fs.readdirSync(storage.pathTo('csv')),
        user: req.session.user
    })
});
router.get('/index-old', checkAuth, (req, res) => {
    res.render('index-old', {
        title: 'scalabot',
        functions: Object.keys(robot).concat('wait'),
        key_taps: key_taps,
        recipes: fs.readdirSync(storage.pathTo('recipes')),
        scenarios: fs.readdirSync(storage.pathTo('scenarios')),
        csv: fs.readdirSync(storage.pathTo('csv')),
        user: req.session.user
    })
});
router.get('/init', (req, res) => {
    var data = {
        functions: Object.keys(robot).concat('wait'),
        key_taps: key_taps
    }
    res.json(data)
});
router.get('/:dir(scenario|recipe|csv)/:file', checkAuth, (req, res) => {
    let dir = req.params.dir;
    let file = req.params.file;
    let load = `load${dir[0].toUpperCase() + dir.slice(1).toLowerCase()}`;
    let scalajson = storage[load](file, false);
    res.json(scalajson || '');
});
router.post('/play', (req, res) => {
    let func = req.body.func;
    let params = req.body.params;
    play.commands([{
        func,
        params
    }], status => {
        io.log('play', status.name, status.data);
        if (status.name === 'ended') {
            res.end('"Done"');
        }
    });
});
router.post('/recipe-play', checkAuth, (req, res) => {
    let scalajson = storage.loadRecipe(req.body.file);
    play.commands(scalajson.records, status => {
        if(status.data)
            status.data.file = req.body.file
        io.log('play', status.name, status.data);
        if (status.name === 'ended') {
            res.end('"Done"');
        }
    });
    res.end('"Done"');
});

router.get("/get-scenario-steps", checkAuth, (req, res) => {
    let poScenario = storage.loadScenario(req.query.file);
    let poCSV = storage.loadCsv(poScenario.csv);

    var files = []
    if (poScenario.start)
        files.push(poScenario.start)

    let recipies = storage.loadReceipeHash(poScenario);
    let entities = map.recordsByEntity(poCSV, recipies, poScenario.flatten || poScenario.recipes, poScenario.transitions);

    let last = _.flatten(entities.map(e => e.commands)).map(c => c.file).reduce((p, c) => {
        if (p !== c)
            files.push(p)

        return c
    })
    if (_.last(files) !== last)
        files.push(last)

    //remove undefined.
    files = files.filter(r => r)

    if (poScenario.end)
        files.push(poScenario.end)

    return res.json(files)
});

router.post('/scenario-play', checkAuth, (req, res) => {
    let poScenario = storage.loadScenario(req.body.file);
    let poCSV = storage.loadCsv(poScenario.csv);

    async.series([
        done => {
            if (!poScenario.start) {
                return done();
            }
            let recipe = storage.loadRecipe(poScenario.start);

            if (!recipe) {
                return done();
            }
            io.log('play', recipe.records, poScenario.start);
            play.commands(recipe.records, status => {
                io.log('play', status.name, status.data);
                if (status.name === 'ended') {
                    done();
                }
            });
        },
        done => {
            let recipies = storage.loadReceipeHash(poScenario);
            let entities = map.recordsByEntity(poCSV, recipies, poScenario.flatten || poScenario.recipes, poScenario.transitions);


            async.mapSeries(entities, (entity, done) => {
                play.commands(entity.commands, status => {
                    io.log('play', status.name, status.data);
                    emitMouse();
                    if (status.name === 'ended') {
                        done();
                    }
                });
            }, done);
        },
        done => {
            if (!poScenario.end) {
                return done();
            }

            let recipe = storage.loadRecipe(poScenario.end);
            if (!recipe) {
                return done();
            }
            play.commands(recipe.records, status => {
                io.log('play', status.name, status.data);
                if (status.name === 'ended') {
                    done();
                }
            });
        }
    ]);

    res.end('"Done"');
});
router.post('/scenario/:file', checkAuth, (req, res) => {
    storage.writeScenario(req.params.file, req.body.scenario);
    res.end('Ended');
});
router.post('/stop', checkAuth, (req, res) => {
    clearTimeout(timer);
    res.end('"Done"');
});
router.post('/recipe', checkAuth, (req, res) => {
    storage.writeRecipe(req.body.file, req.body);
    res.end();
});
router.post('/csv/:file', checkAuth, (req, res) => {
    storage.writeCsv(req.params.file, req.body.csv);
    res.end();
});
router.delete('/recipe', checkAuth, (req, res) => {
    let scalafile = storage.pathTo('recipes', req.body.file);
    fs.unlink(scalafile);
    res.end('Not yet implemented');
});

function emitMouse() {
    // Get mouse position.
    let mouse = robot.getMousePos();

    // Get pixel color in hex format.
    let hex = robot.getPixelColor(mouse.x, mouse.y);
    io.emit('mouse', "#" + hex + ";pos:" + mouse.x + "," + mouse.y);
}

module.exports = router;
