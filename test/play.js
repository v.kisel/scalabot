'use strict';

const sinon = require('sinon');
const path = require('path');
const play = require('../modules/play');
const storage = require('../modules/storage');

//use bluebird instead of system's default
let sandbox = sinon.sandbox.create();
let robot = require('robotjs');
describe('play', function () {
    before(() => {
        // Read data from mocks directory
        sandbox.stub(storage, 'pathTo').callsFake(function () {
            let args = [].slice.call(arguments);
            return path.join.apply(this, [__dirname, 'mocks'].concat(args));
        });
        sandbox.stub(robot, 'typeStringDelayed');
        play.setRobot(robot);
        play.setTimeout(10);
    });

    it('entities', done => {

        done();
    });

    it('commands', done => {
        var recipe = storage.loadRecipe('entity.json');
        sandbox.stub(robot, 'keyTap').callsFake(params => {
            params.should.equal('enter');
        });
        play.commands(recipe.records, status => {
            
            if (status.name==='ended'){
                robot.keyTap.called.should.equal(true);
                robot.typeStringDelayed.called.should.equal(true);
                done();
            }
        });
    }).timeout(20000);


    after(() => sandbox.restore());
});
