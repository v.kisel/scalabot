'use strict';

const sinon = require('sinon');
const path = require('path');
const map = require('../modules/map.js');
const storage = require('../modules/storage.js');
let sandbox = sinon.sandbox.create();

describe('map', function () {
    before(() => {
        // Read data from mocks directory
        sandbox.stub(storage, 'pathTo').callsFake(function(){
            let args = [].slice.call(arguments);
            return path.join.apply(this, [__dirname, 'mocks'].concat(args));
        });
    });

    it('recordsByEntity', () => {
        let csv = storage.loadCsv('entities.csv');
        let scenario = storage.loadScenario('entities.json');
        let recipeHash = storage.loadReceipeHash(scenario);
        let entities = map.recordsByEntity(csv, recipeHash, scenario.flatten || scenario.recipes, scenario.transitions);
        entities.length.should.equal(3);
    
        // The commands should be merged with the data
        let stringifiedJson = JSON.stringify(entities, null, 4);
        stringifiedJson.should.not.match(/\[\[\d\]\]/);
        
        // 9 records = entity + entitytoline + line + linetoextra + extra
        entities[0].commands.length.should.equal(9);
    });

    after(() => {
        sandbox.restore();
    });
});
