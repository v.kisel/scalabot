var mongoose = require('mongoose'),
  db = mongoose.connection,
  DB_URL = 'mongodb://localhost/scalabot'

mongoose.connect(DB_URL)

// 连接成功
db.once('open', () => console.log('Mongo connection successed' + DB_URL))

var Schema = mongoose.Schema

var UserSchema = new Schema({
  username: { type: String, unique: true },
  password: String,
  logindate: { type: Date, default: Date.now }
})

const Models = {
  User: mongoose.model('User', UserSchema)
}

function initAccount() {
  let newAccount = new Models.User({
    username: 'aaa',
    password: '123',
    logindate: new Date()
  })
  newAccount.save((err, data) => {
    if (err) {
      // res.send(err)
    } else {
      // res.send('createAccount successed' + data)
    }
  })
}
initAccount()

module.exports = Models
