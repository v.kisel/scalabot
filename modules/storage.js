const jsonfile = require('jsonfile');
const fs = require('fs');
const path = require('path');


/**
 * Loads recipe from disk
 * @param file
 * @returns {*}
 */
exports.loadRecipe = (file, useArray = true) => {
    let recipe = jsonfile.readFileSync(this.pathTo('recipes', file));
    recipe.records = recipe.records.map(r => {
        var params = r.params.replace('\\t', '\t').replace('\\n', '\n')
        if (useArray)
            params = params.split(/\s*,\s*/);

        let func = r.func;
        return {
            func,
            params
        };
    });
    return recipe;
};

exports.loadScenario = file => jsonfile.readFileSync(this.pathTo('scenarios', file));

exports.loadCsv = file => {
    let csv_string = fs.readFileSync(this.pathTo('csv', file), 'utf8');
    let csv_records = csv_string.split(/\r?\n/g).map(r => r.substring(1, r.length - 1).split(/"?\s*,\s*"?/g)).filter(r => r[0] !== '');
    return csv_records;
};

exports.writeCsv = (file, csv) => {
    let csv_file = this.pathTo('csv', req.params.file);
    let csv_string = csv.map(r => '"' + r.map(c => `${c}`).join('","') + '"').join('\n');
    fs.writeFile(csv_file, csv_string, 'utf8');
};

exports.writeScenario = (file, scenario) => {
    let scalafile = this.pathTo('scenarios', file);
    jsonfile.writeFile(scalafile, JSON.parse(scenario), err => err && console.warn(err));
};

exports.writeRecipe = (file, recipe) => {
    let scalafile = this.pathTo('recipes', file);
    jsonfile.writeFile(scalafile, recipe, err => err && console.warn(err));
};

exports.pathTo = function () {
    let args = [].slice.call(arguments);
    return path.join.apply(this, [__dirname, '..'].concat(args));
};

exports.loadReceipeHash = scenario => {

    let flatten = scenario.flatten || scenario.recipes;
    let recipes = {};

    if (flatten)
        Object.keys(flatten).forEach(k => {
            recipes[flatten[k]] = exports.loadRecipe(flatten[k]);
        });

    let transitions = scenario.transitions;
    if (transitions)
        Object.keys(transitions).forEach(k => {
            recipes[transitions[k]] = exports.loadRecipe(transitions[k]);
        });

    return recipes;
}

exports.loadReceipeHashFull = scenario => {

    let recipes = {};

    let flatten = scenario.flatten || scenario.recipes;
    if (flatten)
        Object.keys(flatten).forEach(k => {
            recipes[flatten[k]] = exports.loadRecipe(flatten[k]);
        });

    let transitions = scenario.transitions;
    if (transitions)
        Object.keys(transitions).forEach(k => {
            recipes[transitions[k]] = exports.loadRecipe(transitions[k]);
        });

    let hooks = scenario.hooks;
    if (hooks)
        Object.keys(hooks).forEach(k => {
            recipes[hooks[k]] = exports.loadRecipe(hooks[k]);
        });

    return recipes;
}
