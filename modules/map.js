/**
 * Matches commands in complete entities. So an entity can be run as a single command
 *
 * @param dataset - records
 * @param flatten -
 * @param transitions
 * @param prev_recipe
 */
exports.recordsByEntity = (dataset, recipes, flatten, transitions) => {
    // returns all records grouped by entity
    flatten = flatten || {};
    let prev_recipe_file = '';
    let entities = dataset.reduce((entities, row, index, dataset) => {
        let recipe_file = exports.matchRecipeFile(flatten, dataset[index], index && dataset[index - 1]);
        let recipe = recipes[recipe_file];
        let commands = recipe.records.map(r => exports.record(r, row, recipe.file));

        // Combines part of the entity
        let key = prev_recipe_file + ">" + recipe_file;
        let transition_file = transitions[key];
        if (transition_file) {
            commands = recipes[transition_file].records.concat(commands);
        }
        console.log('transition_file', key, transition_file);
        let record_is_a_new_entity = flatten['[]'] === recipe_file;
        if (record_is_a_new_entity) {
            entities[entities.length] = {commands};
        } else {
            
            // Record is part of the existing entity.
            let current = entities.length - 1;
            entities[current].commands = entities[current].commands.concat(commands);
        }

        prev_recipe_file = recipe_file;
        /**
         * Todo: Get save command from the transitions by finding the command
         *  - Any of the flatten structure to the first [] transition is a save command
         *  - If any of the flatten structure is the last record, this is the save transition
         *
         *  Better to make save commands a separate set of commands.
         */
        
        return entities;
    }, []);
    
    return entities;
};

exports.record = (record, row, file) => {
    if (!row) return record;
    let func = record.func;
    let params = record.params;

    // merge rows
    row.forEach((data, i) => {
        params = params.join(',').replace(`[[${i}]]`, data).toLowerCase().split(',');
    });
    return {func, params, file};
};

/**
 * When the previous row uses matching ID's the data entry is part of the
 * same PO
 * @param recipes
 * @param currentRow
 * @param newRow
 * @returns {*}
 */
exports.matchRecipeFile = (recipes, currentRow, prevRow) => {
    // The first row should always return the default recipe
    // Which has an empty array of Matching ID's
    
    if (!prevRow) {
        let no_matching_columns = '[]';
        return recipes[no_matching_columns];
    }
    
    // Check ID arrays from yost strict to least strict
    let colMatches = Object.keys(recipes).map(cols => JSON.parse(cols)).sort((v1, v2) => v1.length > v2.length ? 0 : 1);
    let cols = colMatches.filter(m => matchRowColumns(m, currentRow, prevRow))[0];
    let recipe = recipes[JSON.stringify(cols)];
    // console.log('match', colMatches, prevRow[1], currentRow[1], recipe, cols);
    
    return recipe;
}


/**
 * Matches all columns of current and previous row.
 * @param columns - columns of both rows that should be equal
 * @param currentRow
 * @param prevRow
 * @returns {boolean} - true when all equal
 */
function matchRowColumns(columns, currentRow, prevRow) {
    for (let c of columns) {
        if (currentRow[c] !== prevRow[c]) {
            return false;
        }
    }
    return true;
}