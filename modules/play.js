'use strict'

const async = require('async');
let timer = setTimeout(() => {
}, 0);
let robot = require('robotjs');
let timeout = 2000;
// Made public in order to be able to mock it.

/**
 * Plays the commands and replays the last state
 * @param commands {Array <function>}
 */
exports.commands = (commands, fnStatus) => {
    if (!timer) {
        return
    }
    if (!commands.length) {
        return fnStatus && fnStatus({name: 'ended', data: {message: 'no commands'}});
    }
    async.mapSeries(commands, (command, done) => {
        fnStatus({name: 'play', data: command});
        if (command.func === 'wait') {

            // Robot has no wait command
            var wait_time = parseInt(command.params, 10);
            wait_time = wait_time === 'NaN' ? timeout : wait_time;
            timer = setTimeout(() => done(), wait_time);
        } else {

            // All robot functions
            if (command.func !== undefined) {
                robot[command.func].apply(this, [].concat(command.params));
            }
            timer = setTimeout(() => done(), timeout);
        }
    }, err => {
        fnStatus({name: 'ended', data: err});
    });
};

exports.stop = () => {
    throw "Stop not implemented"
};

/**
 * Only in testmode its allowed to set the robot
 */
if (process.env.NODE_ENV === 'test') {
    exports.setRobot = val => {
        robot = val;
    };
    exports.setTimeout = val => {
        timeout = val;
    };
}
