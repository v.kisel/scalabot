'use strict'

var nextID = 1;
var event_handlers = {};
var key_taps, functions;

function addEvent(control, eventName, handler, options) {
    if (control) return event_handlers[getHash(control)] = {
        control,
        eventName,
        handler,
        options
    };

    console.warn("control is empty")
}

function getHash(ele) {
    var control = ele
    if (typeof ele === 'string')
        control = document.querySelector(control)

    control.hashID = control.hashID || ('hashID_' + (nextID++));
    return control.hashID;
}

function initEvents() {
    Object.keys(event_handlers).forEach(hash => {
        var control = event_handlers[hash].control
        var options = event_handlers[hash].options || {}

        if (typeof control === 'string') {
            if (options.multiple)
                control = document.querySelectorAll(control)
            else
                control = document.querySelector(control)
        }

        let handler = event_handlers[hash]
        if (!control || !handler) return

        if (options.multiple)
            control.forEach(ctl => ctl.addEventListener(handler.eventName, handler.handler))
        else
            control.addEventListener(handler.eventName, handler.handler)
    })
}

/* events */

addEvent(".tabs-container ul.nav li", 'click', (e) => {
    e.preventDefault()
    e.currentTarget.parentElement.querySelector("li.active").classList.remove("active")
    e.currentTarget.classList.add("active")

    var targetTab = e.target.hash
    if (!targetTab) return

    var current = document.querySelector(targetTab)
    if (!current) return
    var parent = current.parentElement

    if (parent)
        parent.querySelector(".tab-pane.active").classList.remove("active")

    current.classList.add("active")
}, {
        multiple: true
    })

addEvent("#navlist li", "click", (e) => {
    var recipeFile = e.target.attributes["data-file"].value
    if (!recipeFile) return
    getRecipe(recipeFile)
}, {
        multiple: true
    })

addEvent("#csv-select", 'change', (e) => {
    json_call({
        path: '/csv/' + e.currentTarget.value
    }, cb => {
        console.log('cb:' + cb)
    })
})

addEvent("#scenario-select", 'change', (e) => {
    e.preventDefault();
    getScenario(e.currentTarget.value)
})

addEvent("#scenario-save", 'click', (e) => {
    var button = e.currentTarget.classList
    var file = document.querySelector('#scenario-select').value
    var scenario = document.querySelector('#codeArea').value
    button.add('disabled')
    json_call({
        method: 'POST',
        path: '/scenario/' + file,
        data: { scenario }
    }, () => {
        button.remove('disabled')
    })
})

addEvent('#run', 'click', () => {
    var file = document.querySelector('#scenario-select').value
    json_call({
        method: 'POST',
        path: '/scenario-play',
        data: { file }
    }, (res) => {
        console.log('res:' + res)
    }, (err) => {
        console.log('err: ' + err)
    })
})

addEvent('#pauseAndPlay', 'click', () => {
    console.log('pauseAndPlay')
})

addEvent('#stop', 'click', () => {
    recipeStop()
})

//modal
addEvent("*[data-toggle='modal']", 'click', (e) => {
    var target = e.currentTarget.getAttribute("data-target")
    var modal = document.querySelector(target)

    if (!modal) return
    var mask = document.createElement("DIV")
    mask.className = "modal-backdrop fade in"
    document.body.append(mask)

    modal.classList.add("in")
    modal.style.display = 'block'
}, {
        multiple: true
    })

addEvent("#btnAddRecipe", 'click', e => {
    e.preventDefault()
    var recipeName = document.querySelector("#recipeName")
    var fileName = recipeName.value
    recipeName.value = ''
    json_call({
        method: "POST",
        path: '/recipe',
        data: {
            file: fileName + ".json",
            records: []
        }
    }, (res) => {
        location.replace('/#recipe=' + fileName)
    }, (err) => {
        e.stopImmediatePropagation()
    })
})

addEvent("*[data-dismiss='modal']", 'click', (e) => {
    document.querySelectorAll(".modal")
        .forEach(m => {
            m.classList.remove("in")
            m.style.display = 'none'
        })

    document.querySelectorAll(".modal-backdrop")
        .forEach(m => {
            m.remove()
        })
}, {
        multiple: true
    })

/* end events */

function initData() {
    getFunctionsAndTaps()
    getScenario(document.querySelector("#scenario-select").value)
    getRecipe(document.querySelector("#navlist li.active a").getAttribute("data-file"))
}

function getRecipe(recipeFile) {
    var url = "/recipe/" + recipeFile
    json_call({
        path: url
    }, recipe => {
        if (commandVM.record.records)
            commandVM.record.records.forEach(r => r.destroyed = true)
        commandVM.$forceUpdate()
        commandVM.$nextTick(() => {
            commandVM.$set(commandVM._data, 'record', recipe)
        })
    })
}

function getFunctionsAndTaps() {
    json_call({
        path: '/init'
    }, (res) => {
        key_taps = res.key_taps
        commandVM.$set(commandVM._data, 'recipeFunc', res.functions)
        document.querySelector("#commands").classList.remove("hide")
    })
}

function getScenario(scen) {
    json_call({
        path: '/scenario/' + scen,
        data: {}
    }, scenario => {
        document.querySelector("#codeArea").value = JSON.stringify(scenario, null, 4)
        json_call({
            path: '/get-scenario-steps?file=' + scen,
            data: {}
        }, visual => {
            console.log('v', visual)
            visuaList(visual)
        })
    })
}

function visuaList(scenario) {
    var codeArr = scenario
    /*var codeArr = []
    for (var p in scenario.transitions) {
        if (scenario.transitions.hasOwnProperty(p)) {
            for (var j of p.split('>')) {
                if (codeArr.indexOf(j) == -1)
                    codeArr.push(j)
            }
        }
    }

    if (scenario.hasOwnProperty('start')) codeArr.unshift('start.json')
    if (scenario.hasOwnProperty('end')) codeArr.push('end.json')*/

    var visualItem = ''
    for (var item of codeArr) {
        visualItem += '<div class="visual-item">' + item + '</div>'
    }

    document.querySelector('#visualist').innerHTML = visualItem
}

function recipePlay(file) {
    console.log('play recipes')
    document.querySelector('#play').classList.add('run')
    json_call({
        method: 'POST',
        path: '/recipe-play',
        data: {
            file
        }
    })
}

function recipeStop() {
    // Todo: The server does not stop
    document.querySelectorAll('#visualist .visual-item').forEach(item => item.classList.remove('runing'))
    document.querySelectorAll('#navlist li a').forEach(a => a.attributes['data-index'].value = 0)
    json_call({
        method: 'POST',
        path: '/stop'
    }, res => {
        console.log('stop', res)
        document.querySelector('#play').classList.remove('run')
    })
}

function scenarioRuning(msg) {
    document.querySelector('#play').classList.add('run')
    console.log('msg:', msg)
    var getMsg = JSON.parse(msg)
    // var getMsg = msg
    var tr = document.querySelectorAll('#commands tr')
    tr.forEach(tr => tr.className = '')

    if (getMsg[0] == 'ended') {
        recipeStop()
        return
    }

    if (!getMsg[1].file) return

    var current = document.querySelector('#navlist a[data-file="' + getMsg[1].file + '"]')
    var index = current.getAttribute('data-index')
    var parent = current.parentElement.classList
    var currentItem = ''

    // control scenario
    document.querySelectorAll('#visualist .visual-item').forEach(item => {
        item.classList.remove('runing')
        if (item.textContent == getMsg[1].file)
            currentItem = item
    })
    if (currentItem)
        currentItem.classList.add('runing')

    // control recipes
    if (!current) return

    if (parent.contains('active')) {
        if (index >= tr.length) return
        tr[index].classList.add('runing')
        current.setAttribute('data-index', parseInt(index) + 1)
    } else {
        getRecipe(getMsg[1].file)
        document.querySelectorAll('#navlist li').forEach(li => {
            li.className = ''
            li.firstElementChild.setAttribute('data-index', 0)
        })
        parent.add("active")
        current.setAttribute('data-index', 1)
        if (index >= tr.length) return
        tr[0].classList.add('runing')
    }
}


initData()
initEvents()


function json_call(options, callback, errorCallback) {
    var o = Object.assign({
        method: 'GET'
    }, options);
    var xhr = new XMLHttpRequest();
    xhr.open(o.method, o.path, true);
    xhr.setRequestHeader("Content-type", "application/json");

    if (o.headers) {
        Object.keys(o.headers).forEach(key => {
            xhr.setRequestHeader(key, o.headers[key]);
        })
    }

    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4) {
            if (xhr.status == 200) {
                try {
                    if (xhr.responseText)
                        var json = JSON.parse(xhr.responseText)
                } catch (err) {
                    // errorCallback && errorCallback(err)
                    console.warn(err);
                }
                callback && callback(json);
            } else {
                errorCallback && errorCallback(new Error(`Error: ${xhr.status} - ${xhr.responseText}`))
            }
        };
    }
    xhr.send(typeof o.data != 'string' ? JSON.stringify(o.data) : o.data);
}


var socket = io()
socket.on('play', msg => {
    document.querySelector('#player').innerText = "Playing: " + msg
    scenarioRuning(msg)
})

socket.on('mouse', mouse => {
    var color = mouse.split(';')[0];
    var pos = mouse.split(';')[1];
    document.querySelector('#mouse').innerText = mouse;
    document.querySelector('#mouse').style.background = color;
})
