(function (ref) {

  var $login = document.querySelector(ref)
  if (!$login) return

  function init() {
    var reqMsg = GetQueryString("error")
    if (reqMsg != null && reqMsg.toString().length > 1) {
      var div = document.createElement('div')
      div.className = 'alert alert-danger alert-dismissible fade in'
      div.textContent = 'Incorrect username or password.'

      var btn = document.createElement('button')
      btn.className = 'close'
      btn.setAttribute('data-dismiss', 'alert')
      btn.innerHTML = '&times;'

      div.appendChild(btn)

      var box = document.querySelector('.login-box')
      box.insertBefore(div, box.childNodes[0])

      btn.addEventListener('click', function (e) {
        e.currentTarget.parentElement.remove()
      })
    }
  }

  function GetQueryString(name) {
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)")
    var r = window.location.search.substr(1).match(reg)
    if (r != null) return unescape(r[2]); return null
  }

  window.onload = init
}('#login'))