var $form = q => Array.from(document.querySelector('#robot-form').querySelectorAll(q));
var mouse_speed = {};
var play_row_timeout = null;

var event_handlers = {
    '#recipes-select': ['click', e => {
        e.preventDefault();
        document.activeElement.blur();
        var file = e.currentTarget.value;
        $form('#filename')[0].value = file;
        json_call({path: '/recipe/' + file}, json => {
            var trs = $form('.recipe-row');
            trs.forEach((tr, i) => {
                setValues(tr, json.records[i]);
            });
        });
    }],
    '#recipe-play': ['click', e => {
        e.preventDefault();
        document.activeElement.blur();
        var file = $form('#filename')[0].value;
        json_call({method: 'POST', path: '/recipe-play', data: {file}});
    }],
    '#recipe-store': ['click', e => {
        e.preventDefault();
        document.activeElement.blur();
        var trs = $form('.recipe-row');
        var records = trs.map(tr => getValues(tr)).filter(d => d.func != 'none');
        var file = $form('#filename')[0].value;
        var data = JSON.stringify({records, file});
        json_call({method: 'POST', path: '/recipe', data: data});
    }],
    '#recipe-delete': ['click', e => {
        e.preventDefault();
        var file = $form('#recipes-select')[0].value;
        json_call({method: 'DELETE', path: '/recipe', data: {file}});
    }],
    '#scenario-play': ['click', e => {
        e.preventDefault();
        document.activeElement.blur();
        var file = $form('#scenario-select')[0].value;
        json_call({method: 'POST', path: '/scenario-play', data: {
            file
        }});
    }],
    '#scenario-store': ['click', e => {
        e.preventDefault();
        var file = $form('#scenario-select')[0].value;
        var scenario = JSON.stringify(JSON.parse($form('#scenario-json')[0].value),null,2);
        json_call({method: 'POST', path: '/scenario/' + file, data:{scenario}});
    }],
    '#scenario-select': ['change', e => {
        e.preventDefault();
        json_call({method: 'GET', path: '/scenario/' + e.currentTarget.value, data: {}}, scenario => {
            console.log('scenario',scenario);
            $form('#scenario textarea')[0].value = JSON.stringify(scenario, null, 4);
        });
    }],
    '#csv-select': ['change', e => {
        e.preventDefault();
        var thead = $form('#csv thead')[0];
        var tbody = $form('#csv tbody')[0];
        json_call({method: 'GET', path: '/csv/' + e.currentTarget.value}, csv => {
            thead.innerHTML =
                '<tr><th>' + new Array(csv[0].length).join(',').split(',').map((c,i) => `[[${i}]]`).join('</th><th>');
                    new Array(csv[0].length).map((c,i) => i).join('</th><th>');
                '</th></tr>';
            tbody.innerHTML =
                '<tr>' +
                    csv.map(r => '<td>'+r.map(c => '<input value="'+c+'" />').join('</td><td>') + '</td>').join('</tr><tr>') +
                '</tr>';
        });
    }],
    '#csv-store': ['click', e => {
        e.preventDefault();
        var trs = $form('#csv tbody tr');
        var csv = trs.map(tr => Array.from(tr.querySelectorAll('input')).map(i => i.value));
        var file = $form('#csv-select')[0].value;
        console.log('csv', csv);
        json_call({method: 'POST', path:'/csv/' + file, data: {csv}});
    }],
    '#stop': ['click', e => {
        e.preventDefault();
        json_call({method: 'POST', path: '/stop'});
    }],
    'button.play-row': ['click', e => {
        e.preventDefault();
        document.activeElement.blur();
        var tr = closest_tag(e.currentTarget, 'tr');
        playRow(tr);
        return false;
    }],
    '.text-value': ['keydown', e => {
        var input = e.currentTarget;
        if (!input.value.match(/\d+,\d+/) || e.keyCode < KEY_LEFT || e.keyCode > KEY_DOWN) {
            return;
        }
        mouse_speed[e.keyCode] = mouse_speed[e.keyCode] || 0;
        mouse_speed[e.keyCode]++;
        var values = input.value.split(',').map(v => parseInt(v, 10));

        switch (e.keyCode) {
            case KEY_DOWN:
                values[1] += mouse_speed[e.keyCode];
                break;
            case KEY_LEFT:
                values[0] -= mouse_speed[e.keyCode];
                break;
            case KEY_RIGHT:
                values[0] += mouse_speed[e.keyCode];
                break;
            case KEY_UP:
                values[1] -= mouse_speed[e.keyCode];
                break;
        }
        input.value = values.join(',');
        if (play_row_timeout) {
            clearTimeout(play_row_timeout);
        }
        play_row_timeout = setTimeout(() => {
            // Reset mouse speed and play row
            var tr = closest_tag(input, 'tr');
            mouse_speed = {};
            playRow(tr);
        }, 500);

    }]
};

// Wireup
Object.keys(event_handlers).forEach(handler => $form(handler).forEach(link => link.addEventListener(event_handlers[handler][0], event_handlers[handler][1])));

function playRow(tr) {
    var data = getValues(tr);
    if (data.func == "none") {
        return console.log('None selected');
    }
    var sendString = 'func=' + data.func + '&params=' + data.params;
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.open("POST", '/play', true);
    xmlhttp.setRequestHeader("Content-Type", "application/X-www-form-urlencoded");
    xmlhttp.send(sendString);
}

function json_call(options, callback) {
    var o = Object.assign({method: 'GET'}, options);
    var xhr = new XMLHttpRequest();
    xhr.open(o.method, o.path, true);
    xhr.setRequestHeader("Content-type", "application/json");
    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4 && xhr.status == 200) {
            try {
                var json = JSON.parse(xhr.responseText);
            } catch (err){
                console.warn(err);
            }
            callback && callback(json);
        }
    };
    xhr.send(typeof o.data != 'string' ? JSON.stringify(o.data) : o.data);
}

function closest_tag(thisTag, tagName) {
    do {
        thisTag = thisTag.parentNode
    }
    while (thisTag.tagName !== tagName.toUpperCase()); // uppercase in HTML, lower in XML
    return thisTag;
}

function getValues(tr) {
    return {
        func: tr.querySelector('select[name="function"]').value,
        params: tr.querySelector('input').value
    }
}

function setValues(tr, data) {
    var data = data || {};
    tr.querySelector('input').value = (data.params || []).join(',').replace('\t', '\\t').replace('\n', '\\n');

    // Todo: fix selection of options
    tr.querySelectorAll('select[name="function"] option').forEach(o => {
        if (o.value == data.func) {
            o.selected = true;
        } else {
            o.selected = false;
        }
    });
}


const KEY_LEFT = 37;
const KEY_UP = 38;
const KEY_RIGHT = 39;
const KEY_DOWN = 40;
