//   Vue.config.devtools = true
Vue.component('v-select', VueSelect.VueSelect);
Vue.component('multiselect', {
    mounted() {},
    props: {
        options: Object,
        selected: Boolean
    },
    template: `<v-select taggable v-model='value' :options="operations" multiple :click="options.change(this, options.index)"></v-select>`,
    data: function () {
        return {
            value: this.options.params.map(key => key.replace("\t", "\\t").replace("\n", "\\n")),
            operations: key_taps.map(key => {
                return {
                    label: key.replace("\t", "\\t").replace("\n", "\\n"),
                    value: key.replace("\\t", "\t").replace("\\n", "\n"),
                }
            })
        }
    },
    computed: {
        currentValue: function () {
            return this.value.map(v => {
                if (typeof v === 'object')
                    return v.value
                return v
            })
        }
    }
})
Vue.component('numberinput', {
    props: {
        options: Object
    },
    template: '<input type="number" v-model="currentValue" @keyup="options.change($event, options.index)">',
    data: function () {
        return {
            currentValue: this.options.params.join(",")
        }
    }
})
Vue.component('textinput', {
    props: {
        options: Object
    },
    template: '<input type="text" v-model="currentValue" @keyup="options.change($event, options.index)">',
    data: function () {
        return {
            currentValue: this.options.params.join(",")
        }
    },
    watch: {
        'options.params' (val, oldValue) {
            this.setCurrentValue(val)
        }
    },
    methods: {
        setCurrentValue(value) {
            if (value === this.currentValue) return
            this.currentValue = value.join(",")
        }
    }
})
window.commandVM = new Vue({
    el: '#commands',
    data: {
        recipeFunc: functions,
        type: {
            'multiselect': ['keyTap', 'keyToggle', 'mouseToggle'],
            'numberinput': ['wait', 'setKeyboardDelay'],
            'textinput': ['typeString', 'typeStringDelayed', 'dragMouse']
        },
        record: {},
    },
    methods: {
        changeRecipeParams(v, index) {
            var value = v.currentValue ? v.currentValue : v.target.value
            if (typeof value !== 'string')
                value = [value].join(',').replace("\\t", "\t").replace("\\n", "\n")

            if (this.record.records[index].params === value)
                return

            this.record.records[index].params = value
        },
        playRow(index) {
            var td = document.querySelectorAll('#commands .td3')[index].classList
            td.add('runing')
            var data = this.record.records[index]
            json_call({
                method: 'POST',
                path: '/play',
                data: {
                    func: data.func,
                    params: data.params
                }
            })
            socket.on('play', msg => {
                var getMsg = JSON.parse(msg)
                if (getMsg[0] == 'ended') {
                    td.remove('runing')
                }
            })
        },
        recipeType(func) {
            var type = this.type
            if (type.multiselect.indexOf(func) > -1) {
                return 'multiselect'
            } else if (type.numberinput.indexOf(func) > -1) {
                return 'numberinput'
            } else {
                return 'textinput'
            }
        },
        addNewRow() {
            var item = {
                func: "dragMouse",
                params: ""
            }
            this.record.records.push(item)
        },
        recipeSave() {
            console.log('save', this.record)
        }
    },
    watch: {
        'record': {
            deep: true,
            handler: function (val, oldValue) {
                if (!oldValue) //first time it gets loaded.
                    return

                //switch recipe
                if (oldValue.file !== val.file)
                    return

                json_call({
                    method: "POST",
                    path: '/recipe',
                    data: this.record
                }, console.log.bind(console))
            }
        }
    },
    mounted() {}
})
